# Rałpałpał 3 updater official by XmoxEL

To download, navigate to your os branch, and download updater.sh or type in your git bash/terminal:
```
git clone -b your_os https://gitlab.com/XmoxEL/rpp3-updater.git
```
You can also download manually:
- linux - https://gitlab.com/XmoxEL/rpp3-updater/tree/linux
- windows - https://gitlab.com/XmoxEL/rpp3-updater/tree/windows
- to configure - https://gitlab.com/XmoxEL/rpp3-updater/tree/universal

After downloading updater navigate to catalog, that updater is set in and run command:
```
chmod +x updater.sh
```

Now open updater and enjoy a latest rpp version!